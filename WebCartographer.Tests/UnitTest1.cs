using System.Numerics;
using NetTopologySuite.Algorithm.Hull;
using NetTopologySuite.Geometries;
using Vintagestory.API.MathTools;
using Vintagestory.GameContent;

namespace WebCartographer.Tests;

public class Tests
{
    private List<BlockEntitySign> _signs = null!;
    private BlockEntityStaticTranslocator _translocator;

    [SetUp]
    public void Setup()
    {
        _signs = new()
        {
            new BlockEntitySign
            {
                Pos = new BlockPos(21, 2, 22, 0)
            },
            new BlockEntitySign
            {
                Pos = new BlockPos(12, 2, 12, 0)
            },
            new BlockEntitySign
            {
                Pos = new BlockPos(25, 2, 26, 0)
            },
            new BlockEntitySign
            {
                Pos = new BlockPos(-15, 2, -16, 0)
            }
        };
        _translocator = new BlockEntityStaticTranslocator
        {
            Pos = new BlockPos(2, 2, -2, 0)
        };
    }

    [Test]
    public void Test1()
    {
        SortSigns();
        Assert.That(_signs.FirstOrDefault().Pos, Is.EqualTo(new BlockPos(12, 2, 12, 0)));
    }

    [Test]
    public void Test3()
    {
        Vector3 startColor = new Vector3(255,106,0);
        Vector3 endColor = new Vector3(0,78,255);
        List<string> gradientColors = new List<string>();

        int num = 2;
        for (var i = 0; i <= num; i++)
        {
            var ratio = i / (float)num;

            var r = (int)(startColor.X + ratio * (endColor.X - startColor.X));
            var g = (int)(startColor.Y + ratio * (endColor.Y - startColor.Y));
            var b = (int)(startColor.Z + ratio * (endColor.Z - startColor.Z));

            gradientColors.Add($"#{r:X2}{g:X2}{b:X2}");
        }
        Console.WriteLine();
    }
    
    [Test]
    public void Test2()
    {
        var geometryFactory = new GeometryFactory();
        var points = new Point[]
        {
            new(0,0)             ,new(2,0),
            new(0,1),new(1,1),new(2,1),
            new(0,2)             ,new(2,2),
            new(0,3),new(1,3),new(3,3),
                        new(1,4)
        };
        var multiPoint = geometryFactory.CreateMultiPoint(points);
        
        var concaveHull = new ConcaveHull(multiPoint)
        {
            HolesAllowed = true
        };
        var geometry = concaveHull.GetHull();
        Console.WriteLine(geometry);
    }

    private void SortSigns()
    {
        _signs.Sort((s1, s2) =>
        {
            var sDist = Math.Abs(s1.Pos.X - _translocator.Pos.X) + Math.Abs(s1.Pos.Y - _translocator.Pos.Y) +
                        Math.Abs(s1.Pos.Z - _translocator.Pos.Z);
            var s2Dist = Math.Abs(s2.Pos.X - _translocator.Pos.X) + Math.Abs(s2.Pos.Y - _translocator.Pos.Y) +
                         Math.Abs(s2.Pos.Z - _translocator.Pos.Z);
            return sDist - s2Dist;
        });
    }
}