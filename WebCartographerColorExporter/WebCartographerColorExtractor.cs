﻿using System;
using System.IO;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Config;
using Vintagestory.API.MathTools;
using Vintagestory.GameContent;

namespace WebCartographerColorExporter;

public class WebCartographerColorExtractor : ModSystem
{
    private ICoreClientAPI? _capi;

    private readonly Random _rand = new(DateTime.Now.Millisecond);

    public IClientNetworkChannel _clientNetworkChannel = null!;
    
    public override void StartClientSide(ICoreClientAPI api)
    {
        _capi = api;
        _capi.ChatCommands.Create("exportcolors")
            .RequiresPlayer()
            .WithDescription("Export a file to generate a map using the WebCartographer")
            .HandleWith(OnColorExport);

        _clientNetworkChannel = _capi.Network.RegisterChannel("webcartographer");
        _clientNetworkChannel.RegisterMessageType(typeof(ExportData));
    }

    private TextCommandResult OnColorExport(TextCommandCallingArgs args)
    {
        var pos = _capi!.World.Player.Entity.Pos.AsBlockPos;
        var exportData = new ExportData();
        ColorMap seasonMap, climateMap;
        int color, tintColor;
        int rndCol;
        string code;
        foreach (var block in _capi.World.Blocks)
        {
            if (block.Code is null) continue;
            code = block.Code.ToString();
            if (exportData.Blocks.ContainsKey(code)) continue;

            var colors = new int[10];
            
            tintColor = GetTintColor(block);
            climateMap = block.ClimateColorMapResolved;
            seasonMap = block.SeasonColorMapResolved;
            
            block.ClimateColorMapResolved = null;
            block.SeasonColorMapResolved = null;
            
            if (block is BlockRequireSolidGround)
            {
                color = _capi.BlockTextureAtlas.GetAverageColor(block.TextureSubIdForBlockColor);
            }
            else if (block is BlockPlant)
            {
                var tallGrassBlock = _capi.World.GetBlock(new AssetLocation("game:tallgrass-tall-free"));
                tintColor = GetTintColor(tallGrassBlock);
                var climateMap2 = tallGrassBlock.ClimateColorMapResolved;
                var seasonMap2 = tallGrassBlock.SeasonColorMapResolved;
                tallGrassBlock.ClimateColorMapResolved = null;
                tallGrassBlock.SeasonColorMapResolved = null;
                
                color = tallGrassBlock.GetColor(_capi, pos);
                
                tallGrassBlock.ClimateColorMapResolved = climateMap2;
                tallGrassBlock.SeasonColorMapResolved = seasonMap2;
            }else
            {
                color = block.GetColor(_capi, pos);
            }
            color = ColorUtil.ColorMultiplyEach(color, tintColor);
            color = ((color & 0xff) << 16) | (((color >> 8) & 0xff) << 8) | (((color >> 16) & 0xff) << 0);

            for (var i = 0; i < 10; i++)
            {
                rndCol = RandomizeColor(color);
                colors[i] = rndCol;
            }
            block.ClimateColorMapResolved = climateMap;
            block.SeasonColorMapResolved = seasonMap;
            exportData.Blocks.Add(code, colors);
        }

        if (_clientNetworkChannel.Connected)
        {
            _clientNetworkChannel.SendPacket(exportData);
            return TextCommandResult.Success("Send color data to server");
        }

        _capi.StoreModConfig(exportData, "blockColorMapping.json");
        return TextCommandResult.Success($"Finished exporting color mapping for WebCartographer: {Path.Combine(GamePaths.ModConfig, "blockColorMapping.json")}");
    }

    private int RandomizeColor(int color)
    {
        var next = _rand.Next(-5, 5);
        var red = Math.Clamp(((byte) (color >> 16 & (uint) byte.MaxValue)) + next, 0, 255);
        var green = Math.Clamp(((byte) (color >> 8 & (uint) byte.MaxValue)) + next, 0, 255);
        var blue = Math.Clamp(((byte) (color & (uint) byte.MaxValue)) + next, 0, 255);
        return -16777216 | red << 16 | green << 8 | blue;
    }

    private int GetTintColor(Block block)
    {
        var tintColor = ColorUtil.WhiteArgb;
        if (block.ClimateColorMapResolved != null)
        {
            var x = (int)(0.65 * block.ClimateColorMapResolved.OuterSize.Width);
            var y = (int)(0.719 * block.ClimateColorMapResolved.OuterSize.Height);
            var pixelPos = (y + block.ClimateColorMapResolved.Padding) * block.ClimateColorMapResolved.OuterSize.Width + x + block.ClimateColorMapResolved.Padding;
            tintColor = block.ClimateColorMapResolved.Pixels[pixelPos];
        }

        if (block.SeasonColorMapResolved != null)
        { 
            var x = 70;
            var y = 6;
            var pixelPos = (y + block.SeasonColorMapResolved.Padding) * block.SeasonColorMapResolved.OuterSize.Width + x + block.SeasonColorMapResolved.Padding;
            var seasonColor = block.SeasonColorMapResolved.Pixels[pixelPos];
            tintColor = ColorUtil.ColorOverlay(tintColor, seasonColor, 0.128f);
        }
        return tintColor;
    }
}