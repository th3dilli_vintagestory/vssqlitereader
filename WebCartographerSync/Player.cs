﻿using Vintagestory.API.MathTools;

namespace WebCartographerSync;

public class Player
{
    public string Name { get; set; }
    public string Uid { get; set; }

    public Vec2i Position { get; set; }

    public Player(string name, string uid, Vec2i position)
    {
        Name = name;
        Uid = uid;
        Position = position;
    }
}