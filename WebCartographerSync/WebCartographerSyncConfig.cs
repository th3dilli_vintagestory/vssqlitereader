﻿namespace WebCartographerSync;

public class WebCartographerSyncConfig
{
    public int? Port { get; set; }

    public string Host { get; set; } = "localhost";

    public string Endpoint { get; set; } = "positions";
}