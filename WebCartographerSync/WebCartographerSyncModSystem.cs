﻿using System;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Vintagestory.API.Common;
using Vintagestory.API.MathTools;
using Vintagestory.API.Server;

namespace WebCartographerSync
{
    public class WebCartographerSyncModSystem : ModSystem
    {
        private const string ConfigFile = "WebCartographerSyncConfig.json";

        private ICoreServerAPI _sapi;

        private HttpListener _httpL;

        private WebCartographerSyncConfig _config;
        
        private PlayerPositions _playerPositions;

        public override bool ShouldLoad(EnumAppSide forSide)
        {
            return forSide == EnumAppSide.Server;
        }

        public override void StartServerSide(ICoreServerAPI sapi)
        {
            _sapi = sapi;

            try
            {
                _config = _sapi.LoadModConfig<WebCartographerSyncConfig>(ConfigFile);

                if (_config == null)
                {
                    _config = new WebCartographerSyncConfig
                    {
                        Port = _sapi.Server.Config.Port + 1
                    };
                    _sapi.StoreModConfig(_config, ConfigFile);
                }
            }
            catch (Exception e)
            {
                _sapi.Logger.Error($"Error loading {ConfigFile}: " + e.StackTrace);
                return;
            }

            _playerPositions = new PlayerPositions();
            _sapi.Event.ServerRunPhase(EnumServerRunPhase.Shutdown, OnShutdown);

            _httpL = new HttpListener();
            _httpL.Prefixes.Add($"http://{_config.Host}:{_config.Port}/{_config.Endpoint}/");

            _httpL.Start();
            _ = Task.Run(AsyncListen);
        }

        private async Task AsyncListen()
        {
            while (_httpL.IsListening)
            {
                HttpListenerContext context = await _httpL.GetContextAsync();
                try
                {
                    await ProcessRequestAsync(context);
                }
                catch (Exception ex)
                {
                    _sapi.Logger.Error("WebCartographerSync  " + ex.StackTrace);
                }
            }
        }

        private async Task ProcessRequestAsync(HttpListenerContext context)
        {
            context.Response.KeepAlive = false;
            if (!context.Request.RawUrl.Equals($"/{_config.Endpoint}"))
            {
                context.Response.StatusCode = 400;
                context.Response.Close();
                return;
            }

            _playerPositions.Players = _sapi.World.AllOnlinePlayers.Select(p =>
                {
                    var pos = ((IServerPlayer)p).Entity.Pos.AsBlockPos;
                    return new Player(p.PlayerName, p.PlayerUID, new Vec2i(pos.X, pos.Z));
                }
            ).ToList();
            
            var playerPositions = JsonConvert.SerializeObject(_playerPositions);

            var bytes = Encoding.UTF8.GetBytes(playerPositions);
            context.Response.StatusCode = 200;
            context.Response.ContentLength64 = bytes.Length;
            context.Response.ContentType = "application/json";
            System.IO.Stream output = context.Response.OutputStream;
            await output.WriteAsync(bytes, 0, bytes.Length);
            output.Close();
            context.Response.Close();
        }

        private void OnShutdown()
        {
            _httpL.Stop();
        }
    }
}