﻿using System.Collections.Generic;

namespace WebCartographerSync;

public class PlayerPositions
{
    public List<Player> Players { get; set; }
}