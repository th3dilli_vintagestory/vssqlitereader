$ErrorActionPreference = 'Stop'
Set-StrictMode -Version Latest

$TYPE = 'stable'
$VERSION = '1.18.15'
$DEST = "$(Get-Location)/cartographer"

$SERVER = "vs_server_$VERSION.tar.gz"
$URL_SERVER = "https://cdn.vintagestory.at/gamefiles/$TYPE/vs_server_linux-x64_$VERSION.tar.gz"

if (-not (Test-Path $SERVER)) {
    Invoke-WebRequest -Uri $URL_SERVER -OutFile $SERVER
    
    if (-not (Test-Path "$DEST/assets")) {
        tar -xzf $SERVER -C $DEST
    }
}


New-Item -ItemType Directory -Path html -Force | Out-Null
New-Item -ItemType Directory -Path "html/data" -Force | Out-Null
New-Item -ItemType Directory -Path map_data -Force | Out-Null

New-Item -ItemType Directory -Path $DEST -Force | Out-Null
New-Item -ItemType Directory -Path "$DEST/cartographerData/ModConfig" -Force | Out-Null
New-Item -ItemType Directory -Path "$DEST/cartographerData/Mods" -Force | Out-Null


if ((Test-Path "serverconfig.json")) {
    Copy-Item -Force "serverconfig.json" "$DEST/cartographerData/serverconfig.json"
} else {
    New-Item -Force -ItemType File "serverconfig.json", "$DEST/cartographerData/serverconfig.json" | Out-Null
}

if ((Test-Path "servermagicnumbers.json")) {
    Copy-Item -Force "servermagicnumbers.json" "$DEST/cartographerData/servermagicnumbers.json"
} else {
    New-Item -Force -ItemType File "$DEST/cartographerData/servermagicnumbers.json" | Out-Null
}

Copy-Item -Force "WebCartographerConfig.json" "$DEST/cartographerData/ModConfig/WebCartographerConfig.json"
Copy-Item -Force "blockColorMapping.json" "$DEST/cartographerData/ModConfig/blockColorMapping.json"


if ((Test-Path "Mods")) {
    Copy-Item -Force -Recurse "Mods" "$DEST/cartographerData"
}

docker compose up cartographer

if ((Test-Path "html/data/world")) {
    Remove-Item -Path "html/data/world" -Recurse | Out-Null
}

Copy-Item -Force -Recurse "./map_data/world" "./html/data/world/"
Copy-Item -Force -Recurse "./map_data/geojson" "./html/data/"

docker compose up webmap -d

Write-Output "Done creating webmap - Check it out at localhost:4242"