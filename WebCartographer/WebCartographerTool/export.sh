#!/bin/bash
set -e

# only supports net7 from now on
# net7
TYPE=stable
VERSION=1.18.15
DEST=$(pwd)/cartographer

# download latest version if not already there
SERVER=vs_server_${VERSION}.tar.gz

URL_SERVER=https://cdn.vintagestory.at/gamefiles/${TYPE}/vs_server_linux-x64_${VERSION}.tar.gz

if [ ! -f "$SERVER" ];
then
  curl -k -o $SERVER $URL_SERVER
  # unpack archive if there is nothing installed yet
  if [ ! -d "$DEST/assets" ];
  then
    tar -xzf $SERVER -C $DEST
  fi
fi

mkdir -p html
mkdir -p html/data
mkdir -p map_data

mkdir -p $DEST
mkdir -p $DEST/cartographerData/ModConfig
mkdir -p $DEST/cartographerData/Mods

# checking for existing files to copy 
if [ -f "serverconfig.json" ];
then
  cp serverconfig.json "$DEST"/cartographerData/serverconfig.json
else
  touch serverconfig.json $DEST/cartographerData/serverconfig.json
fi

if [ -f "servermagicnumbers.json" ];
then
  cp servermagicnumbers.json "$DEST"/cartographerData/servermagicnumbers.json
else
  touch $DEST/cartographerData/servermagicnumbers.json
fi

cp WebCartographerConfig.json $DEST/cartographerData/ModConfig/WebCartographerConfig.json
cp blockColorMapping.json $DEST/cartographerData/ModConfig/blockColorMapping.json

if [ -d "Mods" ];
then
  if [ -d "$DEST/cartographerData/Mods" ];
  then
    rm -rf $DEST/cartographerData/Mods/
  fi
  cp -r Mods $DEST/cartographerData/Mods
fi

# start exporting
docker compose up cartographer

# remove old data and cop over new one
rm -rf ./html/data/world/
cp -r ./map_data/world/ ./html/data/world/
cp -r ./map_data/geojson ./html/data/

# start webmap
docker compose up webmap -d

echo "Done creating webmap - Check it out at http://localhost:4242"
