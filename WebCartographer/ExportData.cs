using System.Collections.Generic;
using ProtoBuf;

namespace WebCartographer;

[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
public class ExportData
{
    public Dictionary<string, int[]> Blocks { get; set; } = new();
}