using Newtonsoft.Json;

namespace WebCartographer;

public class Config
{
    /// <summary>
    /// The color and height information the resulting images will use
    /// Default: 4 => ImageMode.MedievalStyleWithHillShading
    /// </summary>
    public ImageMode Mode { get; set; } = ImageMode.MedievalStyleWithHillShading;

    /// <summary>
    /// Path to a directory where all output files will be placed into
    /// </summary>
    public string OutputDirectory { get; set; } = "";
    
    [JsonIgnore]
    public string OutputDirectoryWorld = "";
    [JsonIgnore]
    public string OutputDirectoryGeojson = "";
    
    /// <summary>
    /// Extract the world topmost blocklayer as images
    /// Default: true
    /// </summary>
    public bool ExtractWorldMap { get; set; } = true;
    
    /// <summary>
    /// Fixe white lines on in game map of the world
    /// It rebuilds the rain map for the entire world '/we rebuildrainmap'
    /// The fixes the DatabaseFile in place
    /// </summary>
    public bool FixWhiteLines { get; set; } = false;
    
    /// <summary>
    /// Extract Traders and Translocators (only repaired ones)
    /// Default: true
    /// </summary>
    public bool ExtractStructures { get; set; } = true;
    
    /// <summary>
    /// FLip the y axis for structures
    /// Default: true
    /// </summary>
    public bool AbsolutePositions { get; set; } = false;
    
    /// <summary>
    /// Export the heighmap along with the world map
    /// Default: true
    /// </summary>
    public bool ExportHeightmap { get; set; } = false;

    /// <summary>
    /// Export signs that use the Automap sign prefixes along with the world map
    /// <AM:BASE>, <AM:MISC>, <AM:SERVER>, <AM:TL>
    /// Default: true
    /// </summary>
    public bool ExportSigns { get; set; } = true;

    /// <summary>
    /// Export signs that dont use special tags along with the world map
    /// Default: false
    /// </summary>
    public bool ExportUntaggedSigns { get; set; } = false;

    /// <summary>
    /// Export signs that use custom Automap sign prefixes along with the world map
    /// <AM:CONTINENT>, <AM:PORT>, etc.
    /// Default: false
    /// </summary>
    public bool ExportCustomTaggedSigns { get; set; } = false;

    /// <summary>
    /// Set the size of the individual image tiles
    /// Must be evenly dividable by 32
    /// </summary>
    public int TileSize { get; set; } = 256;
    
    /// <summary>
    /// The number of zoom levels for the webmap to generate, needs also to be adjusted in the webmap
    /// </summary>
    public int BaseZoomLevel { get; set; } = 9;
    
    /// <summary>
    /// Create the zoom levels required for the web-map needs the output from ExtractWorldMap 
    /// </summary>
    public bool CreateZoomLevels { get; set; } = true;
    
    /// <summary>
    /// Number of concurrent Tasks to use to generate tile images
    /// Defaults to -1 -> all available processors
    /// </summary>
    public int MaxDegreeOfParallelism { get; set; } = -1;
    
    /// <summary>
    /// Start the exporter when the server is ready, else you have to login with the client and type .exportcolors
    /// </summary>
    public bool ExportOnStart { get; set; } = false;

    /// <summary>
    /// Export geojson that can be overlayed on the map to show the version a chunk was generated
    /// </summary>
    public bool ExportChunkVersionMap { get; set; }

    /// <summary>
    /// Kicks everyone from the server and pauses the server for the export and shuts it down after its done
    /// </summary>
    public bool SaveMode { get; set; } = true;

    /// <summary>
    /// Stops the server when done exporting
    /// </summary>
    public bool StopOnDone { get; set; } = true;
}

public enum ImageMode
{
    ColorVariations,                // 0
    ColorVariationsWithHeight,      // 1    
    OnlyOneColor,                   // 2
    ColorVariationsWithHillShading, // 3
    MedievalStyleWithHillShading    // 4
}