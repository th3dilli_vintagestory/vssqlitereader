﻿using System;
using System.IO;
using System.Threading.Tasks;
using Vintagestory.API.Common;
using Vintagestory.API.Config;
using Vintagestory.API.Server;
using Vintagestory.Common;
using Vintagestory.Server;

namespace WebCartographer;

public class WebCartographer : ModSystem
{
    private Config _config = null!;

    private Extractor? _extractor;

    private ICoreServerAPI? _sapi;
    private ServerMain _server = null!;
    public IServerNetworkChannel _serverNetworkChannel = null!;
    private bool _isRunning;
    
    const string worldExtent = """
                         var vsWorldGrid = new ol.tilegrid.TileGrid({
                             extent: [-512000,-512000,512000,512000],
                             origin: [-originX,originZ],
                             resolutions: [512,256,128,64,32,16,8,4,2,1],
                             tileSize: [256, 256]
                         });
                         """;

    public override void StartServerSide(ICoreServerAPI api)
    {
        _sapi = api;
        _server = (ServerMain)_sapi!.World;
        _config = LoadConfig();
        if (_config.ExportOnStart)
        {
            _sapi.Event.ServerRunPhase(EnumServerRunPhase.RunGame, RunAsync);
        }

        _sapi.ChatCommands.GetOrCreate("webcartographer")
            .WithDescription("Manage the WebCartographer mod")
            .WithAlias("webc")
            .RequiresPrivilege(Privilege.controlserver)
            .BeginSubCommand("export")
            .WithDescription("Start the export")
            .HandleWith(_ =>
            {
                if (_isRunning)
                {
                    return TextCommandResult.Success("Export already running.");
                }
                RunAsync();
                return TextCommandResult.Success("Export started");
            })
            .EndSubCommand();
        
        _serverNetworkChannel = _sapi.Network.RegisterChannel("webcartographer");
        _serverNetworkChannel.RegisterMessageType(typeof(ExportData));
        _serverNetworkChannel.SetMessageHandler<ExportData>(OnClientData);
    }

    private void OnClientData(IServerPlayer fromplayer, ExportData exportData)
    {
        if (fromplayer.HasPrivilege("root"))
        {
            _sapi?.StoreModConfig(exportData, "blockColorMapping.json");
            RunAsync();
        }
    }

    private void RunAsync()
    {
        Task.Run(StartExport);
    }

    private void StartExport()
    {
        if (_isRunning) return;
        _isRunning = true;
        string? oldPassword = null;
        EnsureFoldersReady();
        if (_config.SaveMode)
        {
            oldPassword = _sapi!.Server.Config.Password;
            _sapi!.Server.Config.Password = Random.Shared.Next().ToString();
            foreach (var player1 in _sapi.World.AllOnlinePlayers)
            {
                var player = (IServerPlayer)player1;
                player.Disconnect("Exporting the map now");
            }
            _server.Suspend(true, 1000);
        }
        
        _extractor = new Extractor(_server, _config, Mod.Logger);
        
        try
        {
            _extractor!.Run();
        }
        catch (Exception e)
        {
            Mod.Logger.Error(e.Message);
        }

        if (_config.SaveMode)
        {
            _server.Suspend(false);
            _sapi!.Server.Config.Password = oldPassword;
        }

        if (_config.StopOnDone)
        {
            _server.Stop("done");
        }
        _isRunning = false;
        _extractor = null;
    }

    private Config LoadConfig()
    {
        _config = _server.Api.LoadModConfig<Config>("WebCartographerConfig.json");

        if (_config is null)
        {
            Mod.Logger.Warning(
                $"WebCartographerConfig missing: {Path.Combine(GamePaths.ModConfig, "WebCartographerConfig.json")}");
            Mod.Logger.Warning("WebCartographerConfig generated with defaults!!!");

            _config = new Config
            {
                Mode = ImageMode.MedievalStyleWithHillShading,
                OutputDirectory = Path.Combine(GamePaths.DataPath, "webmap")
            };
            _server.Api.StoreModConfig(_config, "WebCartographerConfig.json");
        }

        if (_config.OutputDirectory == string.Empty)
        {
            Mod.Logger.Error("WebCartographerConfig OutputDirectory can not be empty!!! Stopping now");
            _server.Stop("config OutputDirectory is empty");
            Environment.Exit(1);
        }

        if (_config.OutputDirectory == GamePaths.DataPath)
        {
            Mod.Logger.Error("WebCartographerConfig OutputDirectory can not be the data folder!!! Stopping now");
            _server.Stop("config OutputDirectory is data folder");
            Environment.Exit(1);
        }

        // limit to max the processorcount
        _config.MaxDegreeOfParallelism = _config.MaxDegreeOfParallelism == -1 ? Environment.ProcessorCount : _config.MaxDegreeOfParallelism;
        return _config;
    }

    private void EnsureFoldersReady()
    {
        _config.OutputDirectoryWorld = Path.Combine(_config.OutputDirectory, "html", "data", "world");
        _config.OutputDirectoryGeojson = Path.Combine(_config.OutputDirectory, "html", "data", "geojson");

        if (Directory.Exists(_config.OutputDirectory))
        {
            if (Directory.Exists(_config.OutputDirectoryWorld) && _config.ExtractWorldMap)
            {
                Mod.Logger.Notification(
                    "WebCartographerConfig OutputDirectory/world is not empty. Clearing it now...");
                Directory.Delete(_config.OutputDirectoryWorld, true);
            }

            if (Directory.Exists(_config.OutputDirectoryGeojson) && (_config.ExportSigns || _config.ExtractStructures))
            {
                Mod.Logger.Notification(
                    "WebCartographerConfig OutputDirectory/geojson is not empty. Clearing it now...");
                Directory.Delete(_config.OutputDirectoryGeojson, true);
            }
        }
        else
        {
            Directory.CreateDirectory(_config.OutputDirectory);
        }

        if (_config.ExportHeightmap)
        {
            Directory.CreateDirectory(Path.Combine(_config.OutputDirectory, "heightmap"));
        }

        if (_config.ExtractWorldMap)
        {
            Directory.CreateDirectory(Path.Combine(_config.OutputDirectoryWorld,
                _config.BaseZoomLevel.ToString()));
            
            var index = Path.Combine(_config.OutputDirectory, "html", "index.html");
            if (!File.Exists(index))
            {
                var html = Path.Combine(_config.OutputDirectory, "html");
                Directory.CreateDirectory(html);
                CopyDirectory(Path.Combine(((ModContainer)Mod).FolderPath, "html"),html, true);
            }
            
            Mod.Logger.Notification($"Savegames DefaultSpawnPosition: {_server.World.DefaultSpawnPosition}");
            
            var orignX = (int)(_server.World.DefaultSpawnPosition?.X ?? _server.WorldMap.MapSizeX / 2f);
            var orignZ = (int)(_server.World.DefaultSpawnPosition?.Z ?? _server.WorldMap.MapSizeZ / 2f);
            var data = worldExtent.Replace("originX", orignX.ToString()).Replace("originZ", orignZ.ToString());
            File.WriteAllText(Path.Combine(_config.OutputDirectory, "html", "worldExtent.js"), data);
        }

        if (_config.CreateZoomLevels)
        {
            for (var i = 0; i < _config.BaseZoomLevel; i++)
            {
                Directory.CreateDirectory(Path.Combine(_config.OutputDirectoryWorld, i.ToString()));
            }
        }

        if (_config.ExportSigns || _config.ExtractStructures)
        {
            Directory.CreateDirectory(_config.OutputDirectoryGeojson);
        }
    }

    static void CopyDirectory(string sourceDir, string destinationDir, bool recursive)
    {
        // Get information about the source directory
        var dir = new DirectoryInfo(sourceDir);

        // Check if the source directory exists
        if (!dir.Exists)
            throw new DirectoryNotFoundException($"Source directory not found: {dir.FullName}");

        // Cache directories before we start copying
        DirectoryInfo[] dirs = dir.GetDirectories();

        // Create the destination directory
        Directory.CreateDirectory(destinationDir);

        // Get the files in the source directory and copy to the destination directory
        foreach (FileInfo file in dir.GetFiles())
        {
            string targetFilePath = Path.Combine(destinationDir, file.Name);
            file.CopyTo(targetFilePath);
        }

        // If recursive and copying subdirectories, recursively call this method
        if (recursive)
        {
            foreach (DirectoryInfo subDir in dirs)
            {
                string newDestinationDir = Path.Combine(destinationDir, subDir.Name);
                CopyDirectory(subDir.FullName, newDestinationDir, true);
            }
        }
    }
}