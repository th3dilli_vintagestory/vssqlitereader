using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Threading;
using Microsoft.Data.Sqlite;
using ProtoBuf;
using Vintagestory.API.Common;
using Vintagestory.API.MathTools;
using Vintagestory.Common;
using Vintagestory.Common.Database;
using Vintagestory.Server;

namespace WebCartographer;

public class SqliteThreadCon
{
    public bool InUse = false;
    public SqliteConnection Con;
    public readonly DbCommand GetMapChunk;
    public readonly DbCommand GetChunk;
    public readonly DbCommand SaveChunksCmd;

    public SqliteThreadCon(SqliteConnection con)
    {
        Con = con;

        GetMapChunk = con.CreateCommand();
        GetMapChunk.CommandText = "SELECT data FROM mapchunk WHERE position=@position";
        GetMapChunk.Parameters.Add(SavegameDataLoader.CreateParameter("position", DbType.UInt64, 0, GetMapChunk));
        GetMapChunk.Prepare();

        GetChunk = con.CreateCommand();
        GetChunk.CommandText = "SELECT data FROM chunk WHERE position=@position";
        GetChunk.Parameters.Add(SavegameDataLoader.CreateParameter("position", DbType.UInt64, 0, GetChunk));
        GetChunk.Prepare();

        SaveChunksCmd = con.CreateCommand();
        SaveChunksCmd.CommandText = "INSERT OR REPLACE INTO mapchunk (position, data) VALUES (@position,@data)";
        SaveChunksCmd.Parameters.Add(SavegameDataLoader.CreateParameter("position", DbType.UInt64, 0, SaveChunksCmd));
        SaveChunksCmd.Parameters.Add(SavegameDataLoader.CreateParameter("data", DbType.Object, null, SaveChunksCmd));
        SaveChunksCmd.Prepare();

        InUse = false;
    }

    public void Free()
    {
        InUse = false;
    }
}

internal class SavegameDataLoader
{
    private readonly SqliteThreadCon[] _sqliteConns;

    private readonly ChunkDataPool _chunkDataPool;

    private readonly ServerMain _server;
    private readonly object _chunkTable = new();
    private readonly object _mapChunkTable = new();
    public readonly ILogger _logger;

    public SavegameDataLoader(ServerMain server, int workers, ILogger modLogger)
    {
        _logger = modLogger;
        _chunkDataPool = new ChunkDataPool(MagicNum.ServerChunkSize, server);
        _server = server;
        _sqliteConns = GetSqlite(_server, workers);
    }

    internal SqliteThreadCon SqliteThreadConn
    {
        get
        {
            lock (_sqliteConns)
            {
                while (true)
                {
                    foreach (var conn in _sqliteConns)
                    {
                        if (conn.InUse) continue;
                        conn.InUse = true;

                        return conn;
                    }
                    Thread.Sleep(500);
                    _logger.Notification("Could not find a free sqlite connection for a worker thread. Waiting...");
                }
            }
        }
    }

    private SqliteThreadCon[] GetSqlite(ServerMain server, int workers)
    {
        var connectionString = new DbConnectionStringBuilder
        {
            { "Data Source", server.Config.WorldConfig.SaveFileLocation },
            { "Pooling", "false" },
            { "Mode", "ReadOnly" }
        }.ToString();

        // + 1 for extract structures where SavegameDataLoader.GetAllServerMapRegions needs one since it does yield return the entire time
        workers++;
        var sqliteConns = new SqliteThreadCon[workers];
        for (var i = 0; i < sqliteConns.Length; i++)
        {
            var sqliteConnection = new SqliteConnection(
                connectionString
            );
            sqliteConnection.Open();
            sqliteConns[i] = new SqliteThreadCon(sqliteConnection);
        }

        _logger.Notification($"Created {sqliteConns.Length} sqlite connections.");
        return sqliteConns;
    }

    public IEnumerable<ChunkPos> GetAllMapChunkPositions(SqliteThreadCon sqliteConn)
    {
        using var cmd = sqliteConn.Con.CreateCommand();

        cmd.CommandText = "SELECT position FROM mapchunk";
        using var sqliteDataReader = cmd.ExecuteReader();
        var posList = new List<ChunkPos>();
        while (sqliteDataReader.Read())
        {
            var pos = (long)sqliteDataReader["position"];
            posList.Add(ChunkPos.FromChunkIndex_saveGamev2((ulong)pos));
        }

        return posList;
    }

    public static IEnumerable<DblChunk<ServerMapRegion>> GetAllServerMapRegions(SqliteThreadCon sqliteConn)
    {
        using var cmd = sqliteConn.Con.CreateCommand();
        cmd.CommandText = "SELECT position, data FROM mapregion";

        using var sqliteDataReader = ReadReaderSafely(cmd);
        while (sqliteDataReader != null && ExecuteReaderSafely(sqliteDataReader))
        {
            ServerMapRegion serverMapRegion;
            ChunkPos position;
            try
            {
                var pos = (long)sqliteDataReader["position"];
                position = ChunkPos.FromChunkIndex_saveGamev2((ulong)pos);
                var obj = sqliteDataReader["data"];
                serverMapRegion = ServerMapRegion.FromBytes(obj as byte[]);
            }
            catch (Exception e)
            {
                ServerMain.Logger.Error(
                    "WebCartographer error while reading MapRegion, region will not be processed.");
                ServerMain.Logger.Error(e.Message);
                continue;
            }

            yield return new DblChunk<ServerMapRegion>(position, serverMapRegion);
        }
    }

    private static SqliteDataReader? ReadReaderSafely(SqliteCommand cmd)
    {
        try
        {
            return cmd.ExecuteReader();
        }
        catch (Exception e)
        {
            ServerMain.Logger.Error(e);
            return null;
        }
    }

    private static bool ExecuteReaderSafely(SqliteDataReader reader)
    {
        try
        {
            return reader.Read();
        }
        catch (Exception e)
        {
            ServerMain.Logger.Error(e);
            return false;
        }
    }

    public void SaveMapChunks(IDictionary<long, ServerMapChunk> toSave)
    {
        var sqliteConn = SqliteThreadConn;
        lock (sqliteConn.Con)
        {
            try
            {
                using var transaction = sqliteConn.Con.BeginTransaction();
                sqliteConn.SaveChunksCmd.Transaction = transaction;
                foreach (var c in toSave)
                {
                    sqliteConn.SaveChunksCmd.Parameters["position"].Value = c.Key;
                    sqliteConn.SaveChunksCmd.Parameters["data"].Value = c.Value.ToBytes();
                    sqliteConn.SaveChunksCmd.ExecuteNonQuery();
                }

                transaction.Commit();
                sqliteConn.Free();
            }
            catch (Exception e)
            {
                _logger.Error("WebCartographer error while saving MapChunks: ");
                _logger.Error(e.Message);
                sqliteConn.Free();
            }
        }
    }

    public ServerMapChunk? GetServerMapChunk(SqliteThreadCon sqliteConn, ulong position)
    {
        sqliteConn.GetMapChunk.Parameters["position"].Value = position;
        using var dataReader = sqliteConn.GetMapChunk.ExecuteReader();
        try
        {
            if (dataReader.Read())
            {
                var bytes = dataReader["data"] as byte[];

                var serverMapChunk = ServerMapChunk.FromBytes(bytes);
                return serverMapChunk;
            }
        }
        catch (Exception e)
        {
            _logger.Error("WebCartographer error while reading ServerMapChunk: ");
            _logger.Error(e);
        }

        return null;
    }

    public ServerMapChunk? GetServerMapChunkT(SqliteThreadCon sqliteConn, ChunkPos position)
    {
        var pos = ChunkPos.ToChunkIndex(position.X, position.Y, position.Z);
        lock (_mapChunkTable)
        {
            return GetServerMapChunk(sqliteConn, pos);
        }
    }

    public ServerMapChunk? GetServerMapChunk(SqliteThreadCon sqliteConn, ChunkPos position)
    {
        var pos = ChunkPos.ToChunkIndex(position.X, position.Y, position.Z);
        return GetServerMapChunk(sqliteConn, pos);
    }

    public ServerChunk? GetServerChunk(SqliteThreadCon sqliteConn, ulong position)
    {
        try
        {
            sqliteConn.GetChunk.Parameters["position"].Value = position;
            using var dataReader = sqliteConn.GetChunk.ExecuteReader();

            if (dataReader.Read())
            {
                var bytes = dataReader["data"] as byte[];
                ServerChunk? serverMapChunk = null;
                serverMapChunk = ServerChunk.FromBytes(bytes, _chunkDataPool, _server);

                return serverMapChunk;
            }
        }
        catch (Exception e)
        {
            _logger.Error("WebCartographer error while saving ServerChunk: ");
            _logger.Error(e.Message);
        }

        return null;
    }

    public ServerChunk? GetServerChunkT(SqliteThreadCon sqliteConn, ChunkPos position)
    {
        var pos = ChunkPos.ToChunkIndex(position.X, position.Y, position.Z);
        lock (_chunkTable)
        {
            return GetServerChunk(sqliteConn, pos);
        }
    }

    public ServerChunk? GetServerChunk(SqliteThreadCon sqliteConn, Vec3i position)
    {
        var pos = ChunkPos.ToChunkIndex(position.X, position.Y, position.Z);
        return GetServerChunk(sqliteConn, pos);
    }

    public SaveGame? GetGameData(SqliteThreadCon sqliteConn)
    {
        try
        {
            using var cmd = sqliteConn.Con.CreateCommand();
            cmd.CommandText = "SELECT data FROM gamedata LIMIT 1";
            using var sqliteDataReader = cmd.ExecuteReader();
            if (sqliteDataReader.Read())
            {
                if (sqliteDataReader["data"] is byte[] data)
                {
                    var saveGame = Serializer.Deserialize<SaveGame>(new MemoryStream(data));
                    return saveGame;
                }
            }

            return null;
        }
        catch (Exception e)
        {
            _logger.Error("Exception thrown on GetGameData: ");
            _logger.Error(e.Message);
            return null;
        }
    }

    public static DbParameter CreateParameter(string parameterName, DbType dbType, object? value, DbCommand command)
    {
        var dbParameter = command.CreateParameter();
        dbParameter.ParameterName = parameterName;
        dbParameter.DbType = dbType;
        dbParameter.Value = value;
        return dbParameter;
    }

    public void Dispose()
    {
        foreach (var con in _sqliteConns)
        {
            con.Con.Dispose();
        }

        _chunkDataPool.FreeAll();
    }
}

public record DblChunk<T>(ChunkPos Position, T Data)
{
    public ChunkPos Position { get; set; } = Position;

    public T Data { get; set; } = Data;
}