using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using NetTopologySuite.Algorithm.Hull;
using NetTopologySuite.Geometries;
using Vintagestory.Common.Database;
using Vintagestory.Server;
using WebCartographer.GeoJson;

namespace WebCartographer;

public struct ChunkPosition
{
    public ChunkPosition(int x, int z)
    {
        X = x;
        Z = z;
    }

    public int X;
    public int Z;
}

public class GroupedPosition
{
    public GroupedPosition(string version, List<ChunkPosition> positions)
    {
        Version = version;
        Positions = positions;
    }

    public string Version { get; set; }
    public List<ChunkPosition> Positions { get; set; }
}

public class GroupChunks
{
    public readonly List<Tuple<int, int>> Directions = new() { Tuple.Create(1, 0), Tuple.Create(-1, 0), Tuple.Create(0, 1), Tuple.Create(0, -1) };

    public readonly List<GroupedPosition> Grouped = new();
    public readonly HashSet<ulong> Visited = new();

    private readonly Dictionary<ChunkPos, string> _positions;
    private readonly Dictionary<ulong, string> _positionsLong;
    private ServerMain _server;
    private readonly int spawnChunkX;
    private readonly int spawnChunkZ;
    private Dictionary<string, string> _colors = new();

    public GroupChunks(Dictionary<ChunkPos, string> positions, ServerMain server)
    {
        _server = server;
        spawnChunkX = (int)(_server.World.DefaultSpawnPosition?.X ?? _server.WorldMap.MapSizeX / 2f);
        spawnChunkZ = (int)(_server.World.DefaultSpawnPosition?.Z ?? _server.WorldMap.MapSizeZ / 2f);

        _positions = positions;
        _positionsLong = new Dictionary<ulong, string>();
        foreach (var (pos, ver) in _positions)
        {
            var key = ChunkPos.ToChunkIndex(pos.X, 0, pos.Z);
            _positionsLong.Add(key, ver);
        }
    }
    
    public void IterativeDfs(int x, int z, string version, ulong key, List<ChunkPosition> group)
    {
        var stack = new Stack<(int x, int z, ulong key)>();
        stack.Push((x, z, key));

        while (stack.Count > 0)
        {
            var (currentX, currentZ, currentKey) = stack.Pop();

            if (Visited.Contains(currentKey)) continue;
            if (!_positionsLong.TryGetValue(currentKey, out var currentVersion) || currentVersion != version) continue;

            Visited.Add(currentKey);
            group.Add(new ChunkPosition(currentX, currentZ));

            foreach (var (dx, dz) in Directions)
            {
                var newX = currentX + dx;
                var newZ = currentZ + dz;
                var newKey = ChunkPos.ToChunkIndex(newX, 0, newZ);

                if (!Visited.Contains(newKey))
                {
                    stack.Push((newX, newZ, newKey));
                }
            }
        }
    }

    public List<GroupedPosition> GroupPositions()
    {
        // Iterate over all positions and perform DFS if the position is not visited
        foreach (var (pos, psvers) in _positions)
        {
            var key = ChunkPos.ToChunkIndex(pos.X, 0, pos.Z);
            if (!Visited.Contains(key))
            {
                var group = new List<ChunkPosition>();
                IterativeDfs(pos.X, pos.Z, psvers, key, group);
                if (group.Count > 0)
                {
                    Grouped.Add(new GroupedPosition(psvers, group));
                }
            }
        }

        return Grouped;
    }

    private List<int> GetGeoJsonCoordinates(Coordinate? pos)
    {
        if (pos == null) return new List<int>();
        var x = (int)pos.X % 32 != 0 ? (int)pos.X+1: (int)pos.X;
        var z = (int)pos.Y % 32 != 0 ? (int)pos.Y+1: (int)pos.Y;

        x = x - spawnChunkX;
        z = (z - spawnChunkZ) * -1;

        return new List<int>
        {
            x, z
        };
    }

    public ChunkVersionFeature GetShape(GroupedPosition gpositions)
    {
        List<ChunkPosition> positions = gpositions.Positions;
        
        var pointAll = new HashSet<Point>();
        foreach (var p in positions)
        {
            var x = p.X * 32;
            var z = p.Z * 32;
            pointAll.Add(new Point(x, z));
            pointAll.Add(new Point(x+31, z));
            pointAll.Add(new Point(x, z+31));
            pointAll.Add(new Point(x+31, z+31));
        }
        // var points = positions.Select(p => new Point(p.X, p.Z)).ToArray();
        var geometryFactory = new GeometryFactory();
        
        var multiPoint = geometryFactory.CreateMultiPoint(pointAll.ToArray());
        
        var concaveHull = new ConcaveHull(multiPoint)
        {
            MaximumEdgeLength = 45
        };
        var geometry = concaveHull.GetHull();
        
        var list = new List<List<int>>();
        foreach (var pos in geometry.Coordinates)
        {
            list.Add(GetGeoJsonCoordinates(pos));
        }

        var re = new List<List<List<int>>> { list };
        var color = _colors[gpositions.Version];
        var feature = new ChunkVersionFeature
        (
            new ChunkVersionProperties(color, gpositions.Version),
            new PolygonGeometry(re)
        );

        return feature;
    }
    
    public void GenerateGradient(List<GroupedPosition> groupedPositions)
    {
        Vector3 startColor = new Vector3(255,106,0);
        Vector3 endColor = new Vector3(0,78,255);
        List<string> gradientColors = new List<string>();

        var versions = groupedPositions.Select(g=>g.Version).Distinct().Select(s => ProperVersion.SemVer.Parse(s)).OrderDescending().ToList();
        var num = versions.Count - 1;
        for (var i = 0; i <= num; i++)
        {
            var ratio = i / (float)num;

            var r = (int)(startColor.X + ratio * (endColor.X - startColor.X));
            var g = (int)(startColor.Y + ratio * (endColor.Y - startColor.Y));
            var b = (int)(startColor.Z + ratio * (endColor.Z - startColor.Z));

            gradientColors.Add($"#{r:X2}{g:X2}{b:X2}");
        }

        for (var index = 0; index < versions.Count; index++)
        {
            var version = versions[index];
            _colors[version.ToString()] = gradientColors[index];
        }
    }
}