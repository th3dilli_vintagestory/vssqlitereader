#  WebCartographer

### Commands
 `/webcartographer export` or `/webc export`

 Use this command to start the export. 
 Best when SaveMode: false,"ExportOnStart": false,"StopOnDone": false, are disabled.
 This way you can join the server and export, maybe useful to test custom tags for signs. 

### Config
```json
{
   // The color and height information the resulting images will use
   // Default: 4 => ImageMode.MedievalStyleWithHillShading
   "Mode": 4,
   // Path to a directory where all output files will be placed into
   "OutputDirectory": "/path/to/img/folder",
   // If you have white lines on the minimap you can use this to get rid of them (rebuild the hight map for all chunks created in 1.18 and up) this will save the fix to the file so you need to use that on the server to fix the ingame minimap as well
   // Default: false
   "FixWhiteLines": false,
   // Extract the world topmost blocklayer as images (256x256 pixels default)
   // Default: true
   "ExtractWorldMap": true,
   // Extract Traders and Translocators (only repaired ones, also exports <AM:TL> tags along with it, if the sign is in the same chunk)
   // Default: true
   "ExtractStructures": true,
   // AbsolutePositions = false => ingame coordinates
   // Default: false
   "AbsolutePositions": false,
   // Export the heightmap along with the world map
   // Default: false
   "ExportHeightmap": false,
   // Export signs that use the Automap sign prefixes along with the world map
   // <AM:BASE>, <AM:MISC>, <AM:SERVER>
   // Default: true
   "ExportSigns": true,
   // Export signs that dont use special tags along with the world map
   // Default: false
   "ExportUntaggedSigns": false,
   // Set the size of the individual image tiles
   // Must be evenly dividable by 32 (untested with other values - webmap is tweaked for it too)
   "TileSize": 256,
   // Export signs that use custom Automap sign prefixes along with the world map
   // <AM:CONTINENT>, <AM:PORT>, etc.
   // Default: false
   "ExportCustomTaggedSigns": false,
   // Export the zoom levels as well for the webmap (now default so you don't need gdal as a seperate step)
   // Default: true
   "CreateZoomLevels": true,
   // The base zoom level to generate (if changed you need to change the webmap as well)
   // Default: 9
   "BaseZoomLevel": 9,
   // Number of concurrent Tasks to use to generate tile images
   // Defaults to -1 -> all available processors
   "MaxDegreeOfParallelism": -1,
   // Kicks everyone from the server and pauses the server for the export and shuts it down after its done
   "SaveMode": true,
   // Start the exporter when the server is ready, else you have to login with the client mod and type .exportcolors
   "ExportOnStart": false,
   // Stops the server when done exporting
   "StopOnDone": true,
   // Export geojson that can be overlayed on the map to show the version a chunk was generated
   "ExportChunkVersionMap": false
}
```

#### Mode
    - 0 => ColorVariations
    - 1 => ColorVariationsWithHeight
    - 2 => OnlyOneColor
    - 3 => ColorVariationsWithHillShading
    - 4 => MedievalStyleWithHillShading
        when using this mode you wont need a blockColorMapping.json

### Usage
1. Add the Webcartographer mod to your server
2. Start the server - it will initialize the config with default values
3. Configure what the mod should do using the generated `Webcartographerconfig.json`
   - by default it will export to the data path of your vs server or client into the "webmap" folder
   - if you want to export when the server starts change `"ExportOnStart": true`
4. If you use a Mode other then 4 you need to add the https://mods.vintagestory.at/wcce to your client and run `.exportcolors` to start a export, this will send the block colors needed to construct the map to the server and start a export. If you use Mode 4 you can still use this to start a export. Once the server has the colors or in Mode 4 you can also use ExportOnStart to export when the server start. You only need to run `.exportcolors` again when changing or updating mods/game to ensure they have the correct color.
5. Wait till it is done, see server console, Note it will by default lock the server and kick players since the server is unusable while exporting
6. In the output directory you will find a html folder that is ready to be hosted using any webserver (nginx, apache, ...) or just open the index.html file in your webbrowser to see the map (markers wont work using that method)


### Configuration

#### Configure the website
Inside the `html` folder is a file called settings.js change it to your liking.


### Client mod
Put the WebCartographerColorExporter_x.y.z.zip into the Mods folder and join your world. Then type `.exportcolors` to start the export. This will export the `blockColorMapping.json` to your client or to the server if the Webcartographer mod is installed.
The exported  `blockColorMapping.json` will be next to your games Mods folder within the ModConfig folder either on the server or local depending if the Webcartographer mod is installed.

### WebMap by Drakker
[vs-webmap by Drakker](https://bitbucket.org/vs-webmap/aurafury-webmap/)

### Demo
[Aura Fury Ancient Paths map](https://map.ap.aurafury.org/?x=0&y=0&zoom=9)

![](assets/preview.png)

#### Feature limitations
##### TL's
The exporter can only find TL's that where generated during world generation.
Manually placed TL's linked with another manually placed would make the exporter significantly slower so it is there for not implemented.
But a manually placed TL linked to a world gen TL will be found.
Further TL's signs have to be now in the same chunk as the TL to be associated with it using `<AM:TL>` tag on first line of that sign.

##### Special Signs
Special signs are signs that are tagged using `<AM:BASE>` , `<AM:MISC>`, `<AM:SERVER>`.Sings will only be exported if the appear on the surface (surface chunk).  Base will use a house as icon while misc uses a small start and server the configured server logo (depends on webmap configuration).
See [vs-webmap by Drakker](https://bitbucket.org/vs-webmap/aurafury-webmap/).

### Performance
Tested on:
i7-8700K,
32 GiB RAM,
1TB SSD Samsung 860 EVO

#### Savegame size: 34.8 GB
Total runtime with all features enabled:
00:22:00

Preparing chunk group data:
00:01:30

Generating the world images:
00:15:30

Exporting Structures:
00:05:00 